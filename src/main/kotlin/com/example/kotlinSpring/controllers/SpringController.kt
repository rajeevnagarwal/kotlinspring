package com.example.kotlinSpring.controllers

import com.example.kotlinSpring.models.Person
import com.example.kotlinSpring.services.PersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
class SpringController {

    @Autowired
    lateinit var personService:PersonService

    @RequestMapping("/info/{path}",method= arrayOf(RequestMethod.GET))
    fun getInfo(@RequestParam id:String,@PathVariable path:String):String{
        return path
    }

    @RequestMapping("/persons",method= arrayOf(RequestMethod.GET))
    fun getPersons(): ArrayList<Person> {

        return personService.getPersons()
    }

    @RequestMapping("/addPerson",method = arrayOf(RequestMethod.POST))
    fun addPerson(@RequestBody person:Person){
        println(person)
    }

}