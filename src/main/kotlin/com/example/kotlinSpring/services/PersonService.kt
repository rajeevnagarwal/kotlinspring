package com.example.kotlinSpring.services

import com.example.kotlinSpring.models.Person
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class PersonService {
    fun getPersons():ArrayList<Person>{
        val person = Person("Rajeev","Nagarwal")
        val person1 = Person("Rajeev","Nagarwal")
        val persons:ArrayList<Person>  = ArrayList()
        persons.add(person)
        persons.add(person1)
        for((firstName,lastName) in persons){
            println()
        }
        return persons
    }
}